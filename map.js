function initmap() {
    // set up the map
    map = new L.Map('mapid');

    // create the tile layer with correct attribution
    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});

    map.setView(new L.LatLng(43.1161355, 1.6094262), 10);
    map.addLayer(osm);
}

initmap();
var coordx;
var coordy;
var marker_process = 0;
var marker=null;

function onMapClick(e) {
    if (marker_process === 0) {

        marker = new L.marker(e.latlng, {draggable: 'true'});

        map.addLayer(marker);
        console.log(map);
        coordx = e.latlng.lat;
        coordy = e.latlng.lng;
        console.log(coordx, coordy);
		marker_process++;
inputAjax(coordx,coordy);

    } 
        marker.on('dragend', function (event) {
            var marker = event.target;
            var position = marker.getLatLng();
            marker.setLatLng(position, {draggable: 'true'}).update();
            coordx = position.lat;
			coordy = position.lng;
		inputAjax(coordx,coordy);
            console.log(coordx, coordy);

        });
};

map.on("click", onMapClick);

function inputAjax(coordx,coordy){
    document.getElementById("x").value = coordx;
    document.getElementById("y").value = coordy;
	$.ajax({
		url:"traitement.php",
		type:"POST",
        timeout: 60000,
		data:{x:coordx, y:coordy},
		datatype:"json",
        success: function traitementCurl(l){
            console.log(l);
        }
	});
}



